import argparse 
import envoy 

class Pacman(object):

  def search(self, term):
    self._run('pacman -Ss {0}'.format(term))

  def install(self, term):
    self._run('pacman -S {0}'.format(term))

  def upgrade(self):
    self._run('pacman -Syu')

  def _run(self, cmd):
    r = envoy.run(cmd)
    print r.std_out
    print r.std_err




def main():
  parser = argparse.ArgumentParser(prog='pacman')
  parser.add_argument('--foo', action='store_true', help='foo help')
  subparsers = parser.add_subparsers(help='sub-command help', dest='subparser')

  parser_search = subparsers.add_parser('se', help='Search packages')
  parser_search.add_argument('term', type=str, help='Package name')

  parser_install = subparsers.add_parser('in', help='Install selected package(s)')
  parser_install.add_argument('pkgname', type=str, help='Package name')

  parser_upgrade = subparsers.add_parser('upg', help='Upgrade all the packages')


  args = parser.parse_args()
  sparser = args.subparser

  pacman = Pacman()

  if sparser == 'se':
    pacman.search(args.term)

  if sparser == 'in':
    pacman.install(args.pkgname)

  if sparser == 'upg':
    pacman.upgrade()
   

if __name__ == '__main__':
  main()

