======================================
Pux - Alternative arguments for Pacman
======================================
Coming from other Linux distributions to Arch Linux, one is to
learn how to use ``pacman``, it's command line pacman management application.
While getting to know with actual commands, sometimes one may want to use 
alternative - possibly more easy to remember commands - like we do with linux
aliases. That is what Pux provides: aliases for pacman commands.

Consider following::

  pacman -Ss python
  pacman -S python-sphinx
  pacman -Syu 

  pux se python
  pux in python-pshinx
  pux upg


